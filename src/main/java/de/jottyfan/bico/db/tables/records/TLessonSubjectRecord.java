/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.bico.db.tables.records;


import de.jottyfan.bico.db.tables.TLessonSubject;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TLessonSubjectRecord extends UpdatableRecordImpl<TLessonSubjectRecord> implements Record3<Integer, Integer, Integer> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.t_lesson_subject.pk_lesson_subject</code>.
     */
    public TLessonSubjectRecord setPkLessonSubject(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.t_lesson_subject.pk_lesson_subject</code>.
     */
    public Integer getPkLessonSubject() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.t_lesson_subject.fk_lesson</code>.
     */
    public TLessonSubjectRecord setFkLesson(Integer value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.t_lesson_subject.fk_lesson</code>.
     */
    public Integer getFkLesson() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>public.t_lesson_subject.fk_subject</code>.
     */
    public TLessonSubjectRecord setFkSubject(Integer value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.t_lesson_subject.fk_subject</code>.
     */
    public Integer getFkSubject() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<Integer, Integer, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<Integer, Integer, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TLessonSubject.T_LESSON_SUBJECT.PK_LESSON_SUBJECT;
    }

    @Override
    public Field<Integer> field2() {
        return TLessonSubject.T_LESSON_SUBJECT.FK_LESSON;
    }

    @Override
    public Field<Integer> field3() {
        return TLessonSubject.T_LESSON_SUBJECT.FK_SUBJECT;
    }

    @Override
    public Integer component1() {
        return getPkLessonSubject();
    }

    @Override
    public Integer component2() {
        return getFkLesson();
    }

    @Override
    public Integer component3() {
        return getFkSubject();
    }

    @Override
    public Integer value1() {
        return getPkLessonSubject();
    }

    @Override
    public Integer value2() {
        return getFkLesson();
    }

    @Override
    public Integer value3() {
        return getFkSubject();
    }

    @Override
    public TLessonSubjectRecord value1(Integer value) {
        setPkLessonSubject(value);
        return this;
    }

    @Override
    public TLessonSubjectRecord value2(Integer value) {
        setFkLesson(value);
        return this;
    }

    @Override
    public TLessonSubjectRecord value3(Integer value) {
        setFkSubject(value);
        return this;
    }

    @Override
    public TLessonSubjectRecord values(Integer value1, Integer value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TLessonSubjectRecord
     */
    public TLessonSubjectRecord() {
        super(TLessonSubject.T_LESSON_SUBJECT);
    }

    /**
     * Create a detached, initialised TLessonSubjectRecord
     */
    public TLessonSubjectRecord(Integer pkLessonSubject, Integer fkLesson, Integer fkSubject) {
        super(TLessonSubject.T_LESSON_SUBJECT);

        setPkLessonSubject(pkLessonSubject);
        setFkLesson(fkLesson);
        setFkSubject(fkSubject);
    }

    /**
     * Create a detached, initialised TLessonSubjectRecord
     */
    public TLessonSubjectRecord(de.jottyfan.bico.db.tables.pojos.TLessonSubject value) {
        super(TLessonSubject.T_LESSON_SUBJECT);

        if (value != null) {
            setPkLessonSubject(value.getPkLessonSubject());
            setFkLesson(value.getFkLesson());
            setFkSubject(value.getFkSubject());
        }
    }
}
