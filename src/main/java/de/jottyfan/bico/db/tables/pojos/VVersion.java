/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.bico.db.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer version;

    public VVersion(VVersion value) {
        this.version = value.version;
    }

    public VVersion(
        Integer version
    ) {
        this.version = version;
    }

    /**
     * Getter for <code>public.v_version.version</code>.
     */
    public Integer getVersion() {
        return this.version;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final VVersion other = (VVersion) obj;
        if (version == null) {
            if (other.version != null)
                return false;
        }
        else if (!version.equals(other.version))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.version == null) ? 0 : this.version.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VVersion (");

        sb.append(version);

        sb.append(")");
        return sb.toString();
    }
}
