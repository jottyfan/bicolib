/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.bico.db.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TSubject implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer pkSubject;
    private final Integer fkSource;
    private final String  theme;
    private final String  subtheme;
    private final String  bookPages;
    private final String  worksheets;
    private final String  bibleverse;
    private final String  notes;
    private final Integer orderNr;

    public TSubject(TSubject value) {
        this.pkSubject = value.pkSubject;
        this.fkSource = value.fkSource;
        this.theme = value.theme;
        this.subtheme = value.subtheme;
        this.bookPages = value.bookPages;
        this.worksheets = value.worksheets;
        this.bibleverse = value.bibleverse;
        this.notes = value.notes;
        this.orderNr = value.orderNr;
    }

    public TSubject(
        Integer pkSubject,
        Integer fkSource,
        String  theme,
        String  subtheme,
        String  bookPages,
        String  worksheets,
        String  bibleverse,
        String  notes,
        Integer orderNr
    ) {
        this.pkSubject = pkSubject;
        this.fkSource = fkSource;
        this.theme = theme;
        this.subtheme = subtheme;
        this.bookPages = bookPages;
        this.worksheets = worksheets;
        this.bibleverse = bibleverse;
        this.notes = notes;
        this.orderNr = orderNr;
    }

    /**
     * Getter for <code>public.t_subject.pk_subject</code>.
     */
    public Integer getPkSubject() {
        return this.pkSubject;
    }

    /**
     * Getter for <code>public.t_subject.fk_source</code>.
     */
    public Integer getFkSource() {
        return this.fkSource;
    }

    /**
     * Getter for <code>public.t_subject.theme</code>.
     */
    public String getTheme() {
        return this.theme;
    }

    /**
     * Getter for <code>public.t_subject.subtheme</code>.
     */
    public String getSubtheme() {
        return this.subtheme;
    }

    /**
     * Getter for <code>public.t_subject.book_pages</code>.
     */
    public String getBookPages() {
        return this.bookPages;
    }

    /**
     * Getter for <code>public.t_subject.worksheets</code>.
     */
    public String getWorksheets() {
        return this.worksheets;
    }

    /**
     * Getter for <code>public.t_subject.bibleverse</code>.
     */
    public String getBibleverse() {
        return this.bibleverse;
    }

    /**
     * Getter for <code>public.t_subject.notes</code>.
     */
    public String getNotes() {
        return this.notes;
    }

    /**
     * Getter for <code>public.t_subject.order_nr</code>.
     */
    public Integer getOrderNr() {
        return this.orderNr;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final TSubject other = (TSubject) obj;
        if (pkSubject == null) {
            if (other.pkSubject != null)
                return false;
        }
        else if (!pkSubject.equals(other.pkSubject))
            return false;
        if (fkSource == null) {
            if (other.fkSource != null)
                return false;
        }
        else if (!fkSource.equals(other.fkSource))
            return false;
        if (theme == null) {
            if (other.theme != null)
                return false;
        }
        else if (!theme.equals(other.theme))
            return false;
        if (subtheme == null) {
            if (other.subtheme != null)
                return false;
        }
        else if (!subtheme.equals(other.subtheme))
            return false;
        if (bookPages == null) {
            if (other.bookPages != null)
                return false;
        }
        else if (!bookPages.equals(other.bookPages))
            return false;
        if (worksheets == null) {
            if (other.worksheets != null)
                return false;
        }
        else if (!worksheets.equals(other.worksheets))
            return false;
        if (bibleverse == null) {
            if (other.bibleverse != null)
                return false;
        }
        else if (!bibleverse.equals(other.bibleverse))
            return false;
        if (notes == null) {
            if (other.notes != null)
                return false;
        }
        else if (!notes.equals(other.notes))
            return false;
        if (orderNr == null) {
            if (other.orderNr != null)
                return false;
        }
        else if (!orderNr.equals(other.orderNr))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.pkSubject == null) ? 0 : this.pkSubject.hashCode());
        result = prime * result + ((this.fkSource == null) ? 0 : this.fkSource.hashCode());
        result = prime * result + ((this.theme == null) ? 0 : this.theme.hashCode());
        result = prime * result + ((this.subtheme == null) ? 0 : this.subtheme.hashCode());
        result = prime * result + ((this.bookPages == null) ? 0 : this.bookPages.hashCode());
        result = prime * result + ((this.worksheets == null) ? 0 : this.worksheets.hashCode());
        result = prime * result + ((this.bibleverse == null) ? 0 : this.bibleverse.hashCode());
        result = prime * result + ((this.notes == null) ? 0 : this.notes.hashCode());
        result = prime * result + ((this.orderNr == null) ? 0 : this.orderNr.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("TSubject (");

        sb.append(pkSubject);
        sb.append(", ").append(fkSource);
        sb.append(", ").append(theme);
        sb.append(", ").append(subtheme);
        sb.append(", ").append(bookPages);
        sb.append(", ").append(worksheets);
        sb.append(", ").append(bibleverse);
        sb.append(", ").append(notes);
        sb.append(", ").append(orderNr);

        sb.append(")");
        return sb.toString();
    }
}
