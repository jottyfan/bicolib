/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.bico.db.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VLessonMissing implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String  sourceName;
    private final String  theme;
    private final String  subtheme;
    private final Integer orderNr;

    public VLessonMissing(VLessonMissing value) {
        this.sourceName = value.sourceName;
        this.theme = value.theme;
        this.subtheme = value.subtheme;
        this.orderNr = value.orderNr;
    }

    public VLessonMissing(
        String  sourceName,
        String  theme,
        String  subtheme,
        Integer orderNr
    ) {
        this.sourceName = sourceName;
        this.theme = theme;
        this.subtheme = subtheme;
        this.orderNr = orderNr;
    }

    /**
     * Getter for <code>public.v_lesson_missing.source_name</code>.
     */
    public String getSourceName() {
        return this.sourceName;
    }

    /**
     * Getter for <code>public.v_lesson_missing.theme</code>.
     */
    public String getTheme() {
        return this.theme;
    }

    /**
     * Getter for <code>public.v_lesson_missing.subtheme</code>.
     */
    public String getSubtheme() {
        return this.subtheme;
    }

    /**
     * Getter for <code>public.v_lesson_missing.order_nr</code>.
     */
    public Integer getOrderNr() {
        return this.orderNr;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final VLessonMissing other = (VLessonMissing) obj;
        if (sourceName == null) {
            if (other.sourceName != null)
                return false;
        }
        else if (!sourceName.equals(other.sourceName))
            return false;
        if (theme == null) {
            if (other.theme != null)
                return false;
        }
        else if (!theme.equals(other.theme))
            return false;
        if (subtheme == null) {
            if (other.subtheme != null)
                return false;
        }
        else if (!subtheme.equals(other.subtheme))
            return false;
        if (orderNr == null) {
            if (other.orderNr != null)
                return false;
        }
        else if (!orderNr.equals(other.orderNr))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.sourceName == null) ? 0 : this.sourceName.hashCode());
        result = prime * result + ((this.theme == null) ? 0 : this.theme.hashCode());
        result = prime * result + ((this.subtheme == null) ? 0 : this.subtheme.hashCode());
        result = prime * result + ((this.orderNr == null) ? 0 : this.orderNr.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VLessonMissing (");

        sb.append(sourceName);
        sb.append(", ").append(theme);
        sb.append(", ").append(subtheme);
        sb.append(", ").append(orderNr);

        sb.append(")");
        return sb.toString();
    }
}
