drop view v_calendar;

create view v_calendar as
SELECT d.slot_day,
  p.forename || ' ' || p.surname AS fullname,
  p.abbreviation,
  o.name AS source_name,
  s.theme,
  s.subtheme,
  s.book_pages,
  s.worksheets,
  s.bibleverse,
  s.notes AS subject_notes,
  l.notes AS lesson_notes,
  d.note AS slot_notes,
  d.pk_slot,
  l.pk_lesson,
  ls.pk_lesson_subject,
  p.pk_person,
  s.pk_subject,
  o.pk_source
FROM public.t_slot d
LEFT JOIN public.t_lesson l ON l.fk_slot = d.pk_slot
LEFT JOIN public.t_lesson_subject ls ON ls.fk_lesson = l.pk_lesson
LEFT JOIN public.t_person p ON p.pk_person = l.fk_person
LEFT JOIN public.t_subject s ON s.pk_subject = ls.fk_subject
LEFT JOIN public.t_source o ON o.pk_source = s.fk_source;

create or replace view v_version as
select 3 as version;
