create table t_profile(
  id int not null primary key generated always as identity,
  username text not null unique,
  theme text default 'light');

create view v_version as
select 2 as version;
