--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4 (Debian 15.4-1.pgdg120+1)
-- Dumped by pg_dump version 15.4 (Debian 15.4-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: t_lesson; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_lesson (
    pk_lesson integer NOT NULL,
    fk_slot integer NOT NULL,
    fk_person integer,
    notes text
);


ALTER TABLE public.t_lesson OWNER TO jotty;

--
-- Name: t_lesson_pk_lesson_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_lesson ALTER COLUMN pk_lesson ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_lesson_pk_lesson_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_lesson_subject; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_lesson_subject (
    pk_lesson_subject integer NOT NULL,
    fk_lesson integer NOT NULL,
    fk_subject integer NOT NULL
);


ALTER TABLE public.t_lesson_subject OWNER TO jotty;

--
-- Name: t_lesson_subject_pk_lesson_subject_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_lesson_subject ALTER COLUMN pk_lesson_subject ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_lesson_subject_pk_lesson_subject_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_slot; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_slot (
    pk_slot integer NOT NULL,
    slot_day date NOT NULL,
    note text
);


ALTER TABLE public.t_slot OWNER TO jotty;

--
-- Name: t_lessonday_pk_lessonday_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_slot ALTER COLUMN pk_slot ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_lessonday_pk_lessonday_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_person; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_person (
    pk_person integer NOT NULL,
    forename text NOT NULL,
    surname text NOT NULL,
    abbreviation text
);


ALTER TABLE public.t_person OWNER TO jotty;

--
-- Name: t_person_pk_person_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_person ALTER COLUMN pk_person ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_person_pk_person_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_source; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_source (
    pk_source integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.t_source OWNER TO jotty;

--
-- Name: t_source_pk_source_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_source ALTER COLUMN pk_source ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_source_pk_source_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_subject; Type: TABLE; Schema: public; Owner: jotty
--

CREATE TABLE public.t_subject (
    pk_subject integer NOT NULL,
    fk_source integer NOT NULL,
    theme text NOT NULL,
    subtheme text,
    book_pages text,
    worksheets text,
    bibleverse text,
    notes text,
    order_nr integer
);


ALTER TABLE public.t_subject OWNER TO jotty;

--
-- Name: t_subject_pk_subject_seq; Type: SEQUENCE; Schema: public; Owner: jotty
--

ALTER TABLE public.t_subject ALTER COLUMN pk_subject ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_subject_pk_subject_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: v_calendar; Type: VIEW; Schema: public; Owner: jotty
--

CREATE VIEW public.v_calendar AS
 SELECT d.slot_day,
    ((p.forename || ' '::text) || p.surname) AS fullname,
    p.abbreviation,
    o.name AS source_name,
    s.theme,
    s.subtheme,
    s.book_pages,
    s.worksheets,
    s.bibleverse,
    s.notes AS subject_notes,
    l.notes AS lesson_notes,
    d.note AS slot_notes
   FROM (((((public.t_slot d
     LEFT JOIN public.t_lesson l ON ((l.fk_slot = d.pk_slot)))
     LEFT JOIN public.t_lesson_subject ls ON ((ls.fk_lesson = l.pk_lesson)))
     LEFT JOIN public.t_person p ON ((p.pk_person = l.fk_person)))
     LEFT JOIN public.t_subject s ON ((s.pk_subject = ls.fk_subject)))
     LEFT JOIN public.t_source o ON ((o.pk_source = s.fk_source)));


ALTER TABLE public.v_calendar OWNER TO jotty;

--
-- Name: v_lesson; Type: VIEW; Schema: public; Owner: jotty
--

CREATE VIEW public.v_lesson AS
 SELECT s.name AS source_name,
    su.theme,
    su.subtheme,
    array_agg(d.slot_day) AS slots,
    su.order_nr
   FROM ((((public.t_subject su
     LEFT JOIN public.t_source s ON ((s.pk_source = su.fk_source)))
     LEFT JOIN public.t_lesson_subject ls ON ((ls.fk_subject = su.pk_subject)))
     LEFT JOIN public.t_lesson l ON ((l.pk_lesson = ls.fk_lesson)))
     LEFT JOIN public.t_slot d ON ((d.pk_slot = l.fk_slot)))
  GROUP BY s.name, su.theme, su.subtheme, su.order_nr;


ALTER TABLE public.v_lesson OWNER TO jotty;

--
-- Name: v_lesson_missing; Type: VIEW; Schema: public; Owner: jotty
--

CREATE VIEW public.v_lesson_missing AS
 SELECT s.name AS source_name,
    su.theme,
    su.subtheme,
    su.order_nr
   FROM (public.t_subject su
     LEFT JOIN public.t_source s ON ((s.pk_source = su.fk_source)))
  WHERE (NOT (su.pk_subject IN ( SELECT t_lesson_subject.fk_subject
           FROM public.t_lesson_subject)));


ALTER TABLE public.v_lesson_missing OWNER TO jotty;

--
-- Name: t_lesson t_lesson_fk_lessonday_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson
    ADD CONSTRAINT t_lesson_fk_lessonday_key UNIQUE (fk_slot);


--
-- Name: t_lesson t_lesson_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson
    ADD CONSTRAINT t_lesson_pkey PRIMARY KEY (pk_lesson);


--
-- Name: t_lesson_subject t_lesson_subject_fk_lesson_fk_subject_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson_subject
    ADD CONSTRAINT t_lesson_subject_fk_lesson_fk_subject_key UNIQUE (fk_lesson, fk_subject);


--
-- Name: t_lesson_subject t_lesson_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson_subject
    ADD CONSTRAINT t_lesson_subject_pkey PRIMARY KEY (pk_lesson_subject);


--
-- Name: t_slot t_lessonday_lesson_day_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_slot
    ADD CONSTRAINT t_lessonday_lesson_day_key UNIQUE (slot_day);


--
-- Name: t_slot t_lessonday_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_slot
    ADD CONSTRAINT t_lessonday_pkey PRIMARY KEY (pk_slot);


--
-- Name: t_person t_person_abbreviation_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_person
    ADD CONSTRAINT t_person_abbreviation_key UNIQUE (abbreviation);


--
-- Name: t_person t_person_forename_surname_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_person
    ADD CONSTRAINT t_person_forename_surname_key UNIQUE (forename, surname);


--
-- Name: t_person t_person_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_person
    ADD CONSTRAINT t_person_pkey PRIMARY KEY (pk_person);


--
-- Name: t_source t_source_name_key; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_source
    ADD CONSTRAINT t_source_name_key UNIQUE (name);


--
-- Name: t_source t_source_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_source
    ADD CONSTRAINT t_source_pkey PRIMARY KEY (pk_source);


--
-- Name: t_subject t_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_subject
    ADD CONSTRAINT t_subject_pkey PRIMARY KEY (pk_subject);


--
-- Name: t_lesson t_lesson_fk_lessonday_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson
    ADD CONSTRAINT t_lesson_fk_lessonday_fkey FOREIGN KEY (fk_slot) REFERENCES public.t_slot(pk_slot);


--
-- Name: t_lesson t_lesson_fk_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson
    ADD CONSTRAINT t_lesson_fk_person_fkey FOREIGN KEY (fk_person) REFERENCES public.t_person(pk_person);


--
-- Name: t_lesson_subject t_lesson_subject_fk_lesson_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson_subject
    ADD CONSTRAINT t_lesson_subject_fk_lesson_fkey FOREIGN KEY (fk_lesson) REFERENCES public.t_lesson(pk_lesson);


--
-- Name: t_lesson_subject t_lesson_subject_fk_subject_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_lesson_subject
    ADD CONSTRAINT t_lesson_subject_fk_subject_fkey FOREIGN KEY (fk_subject) REFERENCES public.t_subject(pk_subject);


--
-- Name: t_subject t_subject_fk_source_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jotty
--

ALTER TABLE ONLY public.t_subject
    ADD CONSTRAINT t_subject_fk_source_fkey FOREIGN KEY (fk_source) REFERENCES public.t_source(pk_source);


--
-- PostgreSQL database dump complete
--

